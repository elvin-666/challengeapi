﻿using Api.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Data
{
    public class CustomerRepository : ContextMemoryRepository<Customer>
    {
        private new DbContextMemory dbContext
        {
            get { return base.dbContext as DbContextMemory; }
        }

        public CustomerRepository(DbContextMemory dbContext) : base(dbContext) { }

    }
}
