﻿using Api.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Api.Data
{
    public class DbContextMemory : DbContext
    {
        public DbContextMemory(DbContextOptions<DbContextMemory> options)
            : base(options) { }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
