﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data
{
    public class ContextMemoryRepository<T> : IContextMemoryRepository<T> where T : class
    {
        protected readonly DbContext dbContext;

        public ContextMemoryRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void AddEntity(T entity)
        {
            this.dbContext.Set<T>().Add(entity);
        }

        public void UpdateEntity(T entity)
        {
            this.dbContext.Set<T>().Update(entity);
        }

        public void DeleteEntity(T entity)
        {
            this.dbContext.Set<T>().Remove(entity);
        }

        public IEnumerable<T> GetAll()
        {
            return this.dbContext.Set<T>().AsQueryable();
        }

        public T Get(int id)
        {
            return this.dbContext.Find<T>(id);
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> where)
        {
            var query = dbContext.Set<T>().AsQueryable();

            if (where != null)
                query = query.Where(where);

            return query;
        }

        public bool Save()
        {
            return this.dbContext.SaveChanges() > 0 ? true : false;
        }
    }

    public interface IContextMemoryRepository<T>
    {
        public T Get(int id);
        public IQueryable<T> Get(Expression<Func<T, bool>> where);
        public IEnumerable<T> GetAll();
        public void AddEntity(T entity);
        public void UpdateEntity(T entity);
        public void DeleteEntity(T entity);
        public bool Save();

    }
}
