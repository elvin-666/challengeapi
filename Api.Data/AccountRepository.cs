﻿using Api.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Api.Data
{
    public class AccountRepository : ContextMemoryRepository<Account>
    {
        private new DbContextMemory dbContext
        {
            get { return base.dbContext as DbContextMemory; }
        }

        public AccountRepository(DbContextMemory dbContext) : base(dbContext) { }
    }
}
