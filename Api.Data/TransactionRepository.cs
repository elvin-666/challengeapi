﻿using Api.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Api.Data
{
    public class TransactionRepository : ContextMemoryRepository<Transaction>
    {
        private new DbContextMemory dbContext
        {
            get { return base.dbContext as DbContextMemory; }
        }

        public TransactionRepository(DbContextMemory dbContext) : base(dbContext) { }
    }
}
