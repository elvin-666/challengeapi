﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Domain.Utilities
{
    public class TaskResult
    {
        private Dictionary<EstatusCode, string> Task { get; set; }
        public KeyValuePair<EstatusCode, string> StatusTask { get; set; }

        public void addError(EstatusCode code, string message)
        {
            Task = new Dictionary<EstatusCode, string>();
            Task.Add(code, message);
        }

        public bool GetStatus()
        {
            if (Task != null && Task.Count > 0)
            {
                StatusTask = Task.FirstOrDefault();
                return false;
            }
            else
                return true;
        }

        public enum EstatusCode
        {
            BandRequest = 400,
            NoFund = 404,
        }
    }
}
