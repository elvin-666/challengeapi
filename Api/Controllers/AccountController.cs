﻿using Api.Data;
using Api.Domain;
using Challenge.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Api.Controllers
{
    [ApiController]
    [Route("/account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;        

        public AccountController(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        [HttpGet("{id:int}", Name = "GetAccount")]
        public ActionResult<Account> Get(int id)
        {
            var customer = _accountService.GetAccount(id);

            if (customer == null)
                return NotFound();

            return customer;
        }

        [HttpPost]
        public ActionResult Post(Account account)
        {
            var task = _accountService.Register(account);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return CreatedAtRoute("GetAccount", new { id = account.Id }, account);
        }

        [HttpPut("{accountId:int}")]
        public ActionResult Put(Account account, int accountId)
        {
            account.Id = accountId;
            var task = _accountService.Update(account);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return Ok(account);
        }


        [HttpPatch("{accountId:int}")]
        public ActionResult Patch(Account account, int accountId)
        {
            account.Id = accountId;
            var task = _accountService.Update(account);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return Ok(account);
        }


        [HttpDelete("{accountId:int}")]
        public ActionResult Delete(int accountId)
        {
            var task = _accountService.Delete(accountId);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);

            return Ok();
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult AddBalance([FromBody] Transaction transaction)
        {
            var task = _accountService.RegisterTrans(transaction);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return CreatedAtRoute("GetAccount", new { id = transaction.Id }, transaction);
        }

        [HttpGet]
        [Route("[action]")]
        public decimal GetBalance(int accountId)
        {
            return _accountService.GetAccountBalance(accountId);
        }
    }
}
