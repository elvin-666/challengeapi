﻿using Api.Data;
using Api.Domain;
using Challenge.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Api.Controllers
{
    [ApiController]
    [Route("/Customer")]
    public class CustomerController : ControllerBase
    {
        private ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            this._customerService = customerService;
        }


        [HttpGet("GetCustomers")]
        public IEnumerable<Customer> Get()
        {
            return _customerService.GetCustomers();
        }


        [HttpGet("{id:int}", Name = "GetCustomer")]
        public ActionResult<Customer> Get(int id)
        {
            var customer = _customerService.GetCustomer(id);

            if (customer == null)
                return NotFound();

            return customer;
        }

        [HttpPost]
        public ActionResult Post(Customer customer)
        {
            var task = _customerService.Register(customer);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return CreatedAtRoute("GetCustomer", new { id = customer.Id }, customer);
        }

        [HttpPut("{customerId:int}")]
        public ActionResult Put(Customer customer, int customerId)
        {
            customer.Id = customerId;
            var task = _customerService.Update(customer);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return Ok(customer);
        }


        [HttpPatch("{customerId:int}")]
        public ActionResult Patch(Customer customer, int customerId)
        {
            customer.Id = customerId;
            var task = _customerService.Update(customer);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);


            return Ok(customer);
        }


        [HttpDelete("{customerId:int}")]
        public ActionResult Delete(int customerId)
        {
            var task = _customerService.Delete(customerId);

            if (!task.GetStatus())
                return StatusCode((int)task.StatusTask.Key, task.StatusTask.Value);

            return Ok();
        }
    }
}
