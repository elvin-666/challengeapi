﻿using Api.Data;
using Api.Domain;
using Api.Domain.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Domain.Utilities.TaskResult;

namespace Challenge.Api.Services
{
    public class CustomerService : ICustomerService
    {
        private CustomerRepository _customerRepository;

        public CustomerService(CustomerRepository customerRepository)
        {
            this._customerRepository = customerRepository;
        }

        /// <summary>
        /// Metodo para obtener toda la información de un cliente
        /// </summary>
        /// <param name="id">Primary key del cliente</param>
        /// <returns></returns>
        public Customer GetCustomer(int id)
        {
            return _customerRepository.Get(id);
        }

        /// <summary>
        /// Metodo para obtener todos los clientes
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Customer> GetCustomers()
        {
            return _customerRepository.GetAll();
        }

        /// <summary>
        /// Metodo para registrar un nuevo cliente en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información del cliente</param>
        /// <returns></returns>
        public TaskResult Register(Customer entity)
        {
            TaskResult TaskResult = new TaskResult();

            if (string.IsNullOrEmpty(entity.Name))
                TaskResult.addError(EstatusCode.BandRequest, "El campo nombre es obligatorio");

            if (string.IsNullOrEmpty(entity.LastName))
                TaskResult.addError(EstatusCode.BandRequest, "El campo apellido es obligatorio");

            if (TaskResult.GetStatus())
            {
                _customerRepository.AddEntity(entity);
                _customerRepository.Save();
            }

            return TaskResult;
        }

        /// <summary>
        /// Metodo para actualizar la información de un cliente
        /// </summary>
        /// <param name="entity">Modelo con la información de un cliente</param>
        /// <returns></returns>
        public TaskResult Update(Customer entity)
        {
            TaskResult TaskResult = new TaskResult();

            var entitydb = GetCustomer(entity.Id);

            if (entitydb == null)
                TaskResult.addError(EstatusCode.NoFund, "El recurso indicado no se encuentra registrado");

            if (string.IsNullOrEmpty(entity.Name))
                TaskResult.addError(EstatusCode.BandRequest, "El campo nombre es obligatorio");

            if (string.IsNullOrEmpty(entity.LastName))
                TaskResult.addError(EstatusCode.BandRequest, "El campo apellido es obligatorio");

            if (TaskResult.GetStatus())
            {
                entitydb.Name = entity.Name;
                entitydb.LastName = entity.LastName;
                entitydb.Direcction = entity.Direcction;
                entitydb.PhoneNumber = entity.PhoneNumber;

                _customerRepository.UpdateEntity(entitydb);
                _customerRepository.Save();
            }

            return TaskResult;
        }

        /// <summary>
        /// Metodo para eliminar un cliente
        /// </summary>
        /// <param name="id">Primary key del cliente</param>
        /// <returns></returns>
        public TaskResult Delete(int id)
        {
            TaskResult TaskResult = new TaskResult();

            var entitydb = GetCustomer(id);

            if (entitydb == null)
                TaskResult.addError(EstatusCode.NoFund, "El recurso indicado no se encuentra registrado");


            if (TaskResult.GetStatus())
            {
                _customerRepository.DeleteEntity(entitydb);
                _customerRepository.Save();
            }

            return TaskResult;
        }
    }

    public interface ICustomerService
    {

        /// <summary>
        /// Metodo para obtener todos los clientes
        /// </summary>
        /// <returns></returns>
        IEnumerable<Customer> GetCustomers();

        /// <summary>
        /// Metodo para obtener toda la información de un cliente
        /// </summary>
        /// <param name="id">Primary key del cliente</param>
        /// <returns></returns>
        Customer GetCustomer(int id);

        /// <summary>
        /// Metodo para registrar un nuevo cliente en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información del cliente</param>
        /// <returns></returns>
        TaskResult Register(Customer entity);

        /// <summary>
        /// Metodo para actualizar la información de un cliente
        /// </summary>
        /// <param name="entity">Modelo con la información de un cliente</param>
        /// <returns></returns>
        TaskResult Update(Customer entity);

        /// <summary>
        /// Metodo para eliminar un cliente
        /// </summary>
        /// <param name="id">Primary key del cliente</param>
        /// <returns></returns>
        TaskResult Delete(int id);
    }
}
