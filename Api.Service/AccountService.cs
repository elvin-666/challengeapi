﻿using Api.Data;
using Api.Domain;
using Api.Domain.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Domain.Utilities.TaskResult;

namespace Challenge.Api.Services
{
    public class AccountService : IAccountService
    {
        private AccountRepository _accountRepository;
        private readonly TransactionRepository _transactionRepository;

        public AccountService(AccountRepository accountRepository, TransactionRepository transactionRepository)
        {
            this._accountRepository = accountRepository;
            this._transactionRepository = transactionRepository;
        }

        /// <summary>
        /// Metodo para obtener una cuenta
        /// </summary>
        /// <param name="id">Primery key de la cuenta</param>
        /// <returns></returns>
        public Account GetAccount(int id)
        {
            return _accountRepository.Get(id);
        }

        /// <summary>
        /// Metodo para obtener el balance de una cuenta
        /// </summary>
        /// <param name="id">Primery key de la cuenta</param>
        /// <returns></returns>
        public decimal GetAccountBalance(int accountId)
        {
            return _transactionRepository.Get(x => x.AccountId == accountId).Sum(x => x.Amount);
        }

        /// <summary>
        /// Metodo para obtener una cuenta
        /// </summary>
        /// <param name="id">Primery key de la cuenta</param>
        /// <returns></returns>
        public Account GetBalance(int id)
        {
            return _accountRepository.Get(id);
        }

        /// <summary>
        /// Metodo para registrar una nueva cuenta en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información de la cuenta</param>
        /// <returns></returns>
        public TaskResult Register(Account entity)
        {
            TaskResult TaskResult = new TaskResult();

            if (string.IsNullOrEmpty(entity.Name))
                TaskResult.addError(EstatusCode.BandRequest, "El campo nombre es obligatorio");


            if (TaskResult.GetStatus())
            {
                _accountRepository.AddEntity(entity);
                _accountRepository.Save();
            }

            return TaskResult;
        }

        /// <summary>
        /// Metodo para actualizar la información de una cuenta
        /// </summary>
        /// <param name="entity">Modelo con la información de la cuenta</param>
        /// <returns></returns>
        public TaskResult Update(Account entity)
        {
            TaskResult TaskResult = new TaskResult();

            var entitydb = GetAccount(entity.Id);

            if (entitydb == null)
                TaskResult.addError(EstatusCode.NoFund, "El recurso indicado no se encuentra registrado");

            if (string.IsNullOrEmpty(entity.Name))
                TaskResult.addError(EstatusCode.BandRequest, "El campo nombre es obligatorio");

            if (entity.CustomerId == 0)
                TaskResult.addError(EstatusCode.BandRequest, "El campo id del cliente es obligatorio");

            if (TaskResult.GetStatus())
            {
                entitydb.Name = entity.Name;
                entitydb.CustomerId = entity.CustomerId;

                _accountRepository.UpdateEntity(entitydb);
                _accountRepository.Save();
            }

            return TaskResult;
        }

        /// <summary>
        /// Metodo para eliminar una cuenta
        /// </summary>
        /// <param name="id">Primary key de la cuenta</param>
        /// <returns></returns>
        public TaskResult Delete(int id)
        {
            TaskResult TaskResult = new TaskResult();

            var entitydb = GetAccount(id);

            if (entitydb == null)
                TaskResult.addError(EstatusCode.NoFund, "El recurso indicado no se encuentra registrado");


            if (TaskResult.GetStatus())
            {
                _accountRepository.DeleteEntity(entitydb);
                _accountRepository.Save();
            }

            return TaskResult;
        }


        /// <summary>
        /// Metodo para registrar una nueva transacción en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información de la transacción</param>
        /// <returns></returns>
        public TaskResult RegisterTrans(Transaction entity)
        {
            TaskResult TaskResult = new TaskResult();

            if (entity.Amount <= 0)
                TaskResult.addError(EstatusCode.BandRequest, "Debe ingresar un monto para la cuenta");

            if (entity.AccountId == 0)
                TaskResult.addError(EstatusCode.BandRequest, "Debe ingresar el primary key de la cuenta");


            if (TaskResult.GetStatus())
            {
                _transactionRepository.AddEntity(entity);
                _transactionRepository.Save();
            }

            return TaskResult;
        }
    }

    public interface IAccountService
    {
        /// <summary>
        /// Metodo para obtener una cuenta
        /// </summary>
        /// <param name="id">Primery key de la cuenta</param>
        /// <returns></returns>
        Account GetAccount(int id);

        /// <summary>
        /// Metodo para registrar una nueva cuenta en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información de la cuenta</param>
        /// <returns></returns>
        TaskResult Register(Account entity);

        /// <summary>
        /// Metodo para registrar una nueva transacción en la base de datos
        /// </summary>
        /// <param name="entity">Modelo con la información de la transacción</param>
        /// <returns></returns>
        TaskResult RegisterTrans(Transaction entity);

        /// <summary>
        /// Metodo para actualizar la información de una cuenta
        /// </summary>
        /// <param name="entity">Modelo con la información de la cuenta</param>
        /// <returns></returns>
        TaskResult Update(Account entity);


        /// <summary>
        /// Metodo para eliminar una cuenta
        /// </summary>
        /// <param name="id">Primary key de la cuenta</param>
        /// <returns></returns>
        TaskResult Delete(int id);

        /// <summary>
        /// Metodo para obtener el balance de una cuenta
        /// </summary>
        /// <param name="id">Primery key de la cuenta</param>
        /// <returns></returns>
        decimal GetAccountBalance(int accountId);
    }
}
